from django.db import models
from django.conf import settings

# Create your models here.

class Post(models.Model):
    titulo = models.CharField(max_length=255)
    conteudo = models.CharField(max_length=255)
    data = models.DateTimeField()
    poster_url = models.URLField(max_length=200, null=True)

    def __str__(self):
        return f'{self.name} ({self.release_year})'