from django.urls import path, re_path

from . import views

app_name = 'posts'
urlpatterns = [
    # path('', views.PostListView.as_view(), name='index'),
    # path('create/', views.PostCreateView.as_view() ,name='create'),
    # path('update/<int:pk>/', views.PostUpdateView.as_view(), name='update'),
    # path('delete/<int:pk>/', views.PostDeleteView.as_view(), name='delete'),
    # path('<int:pk>/', views.PostDetailView.as_view(), name='detail'),
    path('', views.list_posts, name='index'),
    path('list_categories/', views.list_categories, name='list_categories'),
    path('category/<int:category_id>/', views.detail_category, name='detail_category'),
    path('create/', views.create_post ,name='create'),
    path('search/', views.search_posts ,name='search'),
    path('update/<int:post_id>/', views.update_post, name='update'),
    path('delete/<int:post_id>/', views.delete_post, name='delete'),
    path('<int:post_id>/', views.detail_post, name='detail'),
    path('<int:post_id>/review/', views.create_review, name='review'),
]
