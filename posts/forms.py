from django import forms
from .models import Post, Review
from django.forms import ModelForm

'''class PostForm(forms.Form):
    name = forms.CharField(label='Título', max_length=100)
    data = forms.IntegerField(label='Data de Publicação',
                                      min_value=1888)
    conteudo = forms.CharField(label='Conteúdo', max_length=5000)
'''
class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = [
            'name',
            'conteudo',
        ]
        labels = {
            'name': 'Título',
            'conteudo': 'Conteúdo',
        }

class ReviewForm(ModelForm):
    class Meta:
        model = Review
        fields = [
            'author',
            'text',
        ]
        labels = {
            'author': 'Usuário',
            'text': 'Resenha',
        }