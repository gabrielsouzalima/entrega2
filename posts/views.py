from django.shortcuts import render, get_object_or_404
import unidecode 
# Create your views here.
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from .temp_data import post_data
from .models import Post, Review, Category
from django.views import generic
from .forms import PostForm, ReviewForm


'''class PostListView(generic.ListView):
    model = Post
    template_name = 'posts/index.html'

class PostDetailView(generic.DetailView):
    model = Post
    template_name = 'posts/detail.html'

class PostCreateView(generic.CreateView):
    model = Post
    template_name = 'posts/create.html'
    form_class = PostForm
    success_url = reverse_lazy('posts:index')
    
class PostUpdateView(generic.UpdateView):
    model = Post
    template_name = 'posts/update.html'
    fields = ['name', 'conteudo']
    reverse_lazy('posts:index')

class PostDeleteView(generic.DeleteView):
    model = Post
    template_name = 'posts/delete.html'
    success_url = reverse_lazy('posts:index')'''

def search_posts(request):
    context = {}
    if request.GET.get('query', False):
        search_term = request.GET['query'].lower()
        post_list = Post.objects.filter(name__icontains=search_term)
        context = {"post_list": post_list}
    return render(request, 'posts/search.html', context)

def list_posts(request):
    post_list = Post.objects.all()
    context = {'post_list': post_list}
    return render(request, 'posts/index.html', context)

def detail_post(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    context = {'post': post}
    return render(request, 'posts/detail.html', context)

def create_post(request):
    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            post_name = form.cleaned_data['name']
            #post_data = form.cleaned_data['data']
            post_conteudo = form.cleaned_data['conteudo']
            post = Post(name=post_name,
                        conteudo=post_conteudo)
            post.save()
            return HttpResponseRedirect(
                reverse('posts:detail', args=(post.id, )))
    else:
        form = PostForm()
    context = {'form': form}
    return render(request, 'posts/create.html', context)

def update_post(request, post_id):
    post = get_object_or_404(Post, pk=post_id)

    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post.name = form.cleaned_data['name']
            post.conteudo = form.cleaned_data['conteudo']
            post.save()
            return HttpResponseRedirect(
                reverse('posts:detail', args=(post.id, )))
    else:
        form = PostForm(
            initial={
                'name': post.name,
                'conteudo': post.conteudo
            })
    context = {'post': post, 'form': form}
    return render(request, 'posts/update.html', context)


def delete_post(request, post_id):
    post = get_object_or_404(Post, pk=post_id)

    if request.method == "POST":
        post.delete()
        return HttpResponseRedirect(reverse('posts:index'))

    context = {'post': post}
    return render(request, 'posts/delete.html', context)

def create_review(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    if request.method == 'POST':
        form = ReviewForm(request.POST)
        if form.is_valid():
            review_author = form.cleaned_data['author']
            review_text = form.cleaned_data['text']
            review = Review(author=review_author,
                            text=review_text,
                            post=post)
            review.save()
            return HttpResponseRedirect(
                reverse('posts:detail', args=(post_id, )))
    else:
        form = ReviewForm()
    context = {'form': form, 'post': post}
    return render(request, 'posts/review.html', context)

def list_categories(request):
    category_list = Category.objects.all()
    context = {'category_list': category_list}
    return render(request, 'posts/list_categories.html', context)

def detail_category(request, category_id):
    category = get_object_or_404(Category, pk=category_id)
    context = {'category': category}
    return render(request, 'posts/category_detail.html', context)


